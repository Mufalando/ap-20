package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository = new AcademyRepository();

    @InjectMocks
    private AcademyService academyService = new AcademyServiceImpl(academyRepository);

    // TODO create tests

    @Test
    public void TestProduceKnightShouldCallGetAcademyByName(){
        academyService.produceKnight("Lordran","majestic");
        assertTrue(academyService.getKnight() instanceof MajesticKnight);
    }

    @Test
    public void TestGetKnightAcademiesShouldCallGetKnightList(){
        List<KnightAcademy> acadList = new ArrayList<>();
        AcademyService academyServiceSpy = spy(academyService);
        when(academyServiceSpy.getKnightAcademies())
                .thenReturn(acadList);
        Iterable<KnightAcademy> calledAcads = academyServiceSpy.getKnightAcademies();
        assertThat(calledAcads).isEqualTo(acadList);
    }
}
