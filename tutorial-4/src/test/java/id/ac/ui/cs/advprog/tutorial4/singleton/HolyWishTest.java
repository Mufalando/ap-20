package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        // TODO create test
        HolyWish temp = HolyWish.getInstance();
        assertEquals(holyWish,temp);
    }

    @Test
    public void testWish(){
        // TODO create test
        holyWish.setWish("Stay on that day forever");
        assertEquals("Stay on that day forever",holyWish.getWish());
        assertEquals("Stay on that day forever",holyWish.toString());
    }

    @Test
    public void testWishExist(){
        // TODO create test
        HolyWish temp = HolyWish.getInstance();
        holyWish.setWish("Stay on that day forever");
        assertEquals("Stay on that day forever",temp.getWish());
    }

}
