package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyWish holyWish;

    @InjectMocks
    private HolyGrail holyGrail;

    // TODO create tests
    @Test
    public void testHolyGrailSetWish(){
        String wish = "TEST WEH";
        holyGrail.makeAWish(wish);
        verify(holyWish, times(1)).setWish(wish);
    }

    @Test
    public void testHolyGrailGetWish() throws ClassNotFoundException{
        HolyWish holyWish = holyGrail.getHolyWish();
        assertThat(holyWish).isInstanceOf(Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish"));
    }
}
