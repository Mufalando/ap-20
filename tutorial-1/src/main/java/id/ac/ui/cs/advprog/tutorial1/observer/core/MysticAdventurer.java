package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

        public MysticAdventurer(Guild guild) {
                this.name = "Mystic";
                this.guild = guild;
                //ToDo: Complete Me
        }

        @Override
        public void update() {
                if (!guild.getQuestType().equals("Rumble")){
                        this.getQuests().add(guild.getQuest());
                }
        }

        //ToDo: Complete Me
}
