package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    private String type;
    public AttackWithGun(){
        type = "Gun";
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String attack() {
        return "Attack With " + type;
    }
}
