package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    private String Type;

    public DefendWithBarrier(){
        Type = "Barrier";
    }

    @Override
    public String getType() {
        return Type;
    }

    @Override
    public String defend() {
        return "Defend with " + Type;
    }
}
