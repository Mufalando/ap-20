package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    private String Type;

    public DefendWithArmor(){
        Type = "Armor";
    }

    @Override
    public String getType() {
        return Type;
    }

    @Override
    public String defend() {
        return "Defend with " + Type;
    }
}
