package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    private String Type;

    public DefendWithShield(){
        Type = "Shield";
    }

    @Override
    public String getType() {
        return Type;
    }

    @Override
    public String defend() {
        return "Defend with " + Type;
    }
}
