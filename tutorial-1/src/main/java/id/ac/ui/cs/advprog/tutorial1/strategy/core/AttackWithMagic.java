package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    private String type;
    public AttackWithMagic(){
        type = "Magic";
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String attack() {
        return "Attack With " + type;
    }
}
