package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    private String type;
    public AttackWithSword(){
        type = "Sword";
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public String attack() {
        return "Attack With " + type;
    }
}
