package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponTest {

    Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = Mockito.mock(Weapon.class);
    }

    @Test
    public void testMethodGetWeaponName(){
        Mockito.doCallRealMethod().when(weapon).getName();
        String weaponName = weapon.getName();
        //TODO: Complete me
        Mockito.verify(weapon,Mockito.atLeast(1)).getName();
    }

    @Test
    public void testMethodGetWeaponDescription(){
        Mockito.doCallRealMethod().when(weapon).getDescription();
        String weaponDescription = weapon.getDescription();
        Mockito.verify(weapon,Mockito.atLeast(1)).getDescription();
        //TODO: Complete me
    }

    @Test
    public void testMethodGetWeaponValue(){
        Mockito.doCallRealMethod().when(weapon).getWeaponValue();
        int weaponValue = weapon.getWeaponValue();
        Mockito.verify(weapon,Mockito.atLeast(1)).getWeaponValue();
        //TODO: Complete me
    }
}
