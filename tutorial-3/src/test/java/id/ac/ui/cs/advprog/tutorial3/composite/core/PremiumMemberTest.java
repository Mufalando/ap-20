package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member temp = new OrdinaryMember("Aku","ASIK");
        member.addChildMember(temp);
        assertEquals(temp,member.getChildMembers().get(0));
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member temp = new OrdinaryMember("Aku","ASIK");
        member.addChildMember(temp);
        member.removeChildMember(temp);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member temp = new OrdinaryMember("Aku","ASIK");
        for (int i=0;i<4;i++){
            member.addChildMember(temp);
        }
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member temp2 = new PremiumMember("SAY","Master");
        Member temp = new OrdinaryMember("Aku","ASIK");
        for (int i=0;i<4;i++){
            temp2.addChildMember(temp);
        }
        assertEquals(4,temp2.getChildMembers().size());
    }
}
