package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        Member temp = new OrdinaryMember("Aku","ASIK");
        guild.addMember(guildMaster,temp);
        assertEquals(2,guild.getMemberList().size());
        assertEquals(1,guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member temp = new OrdinaryMember("Aku","ASIK");
        guild.addMember(guildMaster,temp);
        guild.removeMember(guildMaster,temp);
        assertEquals(1,guild.getMemberList().size());
        assertEquals(0,guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member temp = new OrdinaryMember("Aku","ASIK");
        guild.addMember(guildMaster,temp);
        assertEquals(guildMaster,guild.getMemberList().get(0));
        assertEquals(temp,guild.getMemberList().get(1));
    }
}
