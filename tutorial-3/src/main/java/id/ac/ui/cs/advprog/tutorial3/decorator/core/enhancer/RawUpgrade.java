package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        Random rand = new Random();
        int temp =  5 +  rand.nextInt(6);
        weaponValue = weapon.getWeaponValue() + temp;
        return this.weaponValue;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return weapon.getDescription();
    }
}
