package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
        //TODO: Complete me
    private String name;
    private String role;
    List<Member> childMember;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        childMember = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public void addChildMember(Member member) {
        if (childMember.size()>=3 && !role.equals("Master")){
            return;
        }
        childMember.add(member);
    }

    @Override
    public void removeChildMember(Member member) {
        childMember.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return childMember;
    }
}
